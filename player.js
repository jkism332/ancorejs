player$ = class extends Thing {
  constructor() {
    super();
    this.x = 200;
    this.y = 200;
    this.width = 40;
    this.height = 40;
    this.aa = 0;
    this.angle = 45;
    this.xspd = this.yspd = 0;
    this.speed = 6;
    this.depth = 15;
    this.mask = Polygon.new_rect(this.width, this.height);
  }
  step() {
    if (Key.check("W")) {
      this.yspd = -1;
    } else if (Key.check("S")) {
      this.yspd = 1;
    } else {
      this.yspd = 0;
    }

    if (Key.check("W") && Key.check("S")) {
      this.yspd = 0;
    }

    if (Key.check("A")) {
      this.xspd = -1;
    } else if (Key.check("D")) {
      this.xspd = 1;
    } else {
      this.xspd = 0;
    }
    if (Key.check("A") && Key.check("D")) {
      this.xspd = 0;
    }


    /*this.aa = Angle.between(
      { x: this.x, y: this.y },
      { x: Mouse.X, y: Mouse.Y }
    );
    this.angle = Angle.relp(this.angle, this.aa, 0.05);*/
    while (place_meeting(this, obj.enemy).is) {
      this.x -= this.xspd;
      this.y -= this.yspd;
    }

    if (place_meeting(this, obj.enemy, this.x + this.speed * this.xspd).is) {
      while (!(place_meeting(this, obj.enemy, this.x + this.xspd).is)) {
        this.x += this.xspd;
      }
      this.xspd = 0;
    }
    if (place_meeting(this, obj.enemy, this.x, this.y + this.speed * this.yspd).is) {
      while (!(place_meeting(this, obj.enemy, this.x, this.y + this.yspd).is)) {
        this.y += this.yspd;
      }
      this.yspd = 0;
    }
    this.x += this.speed * this.xspd;
    this.y += this.speed * this.yspd; 
    this.oldangle = this.angle;
  }
  draw() {
    this.show_collision_masks("blue");
  }
};

obj.player = [];
