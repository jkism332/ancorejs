enemy$ = class extends Thing {
  constructor() {
    super();
    this.x = 300;
    this.y = 300;
    this.width = 40;
    this.height = 40;
    this.angle = 25;
    this.depth = 1000;
    this.mask = Polygon.new_rect(this.width, this.height);
  }
  step() {
  }
  draw() {
    this.show_collision_masks();
  }
}

obj.enemy = [];